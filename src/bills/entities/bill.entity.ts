/* eslint-disable prettier/prettier */
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"

export type ReceiptItem = {
    id: number;
    name: string;
    slcSubCate: number;
    subCategory: string;
    slcSweetLvl: number;
    sweetLevel: string;
    price: number;
    unit: number;
    productId: number;
    product?: string;
}
@Entity()
export class Bill {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    queue?: number;

    @Column()
    createDate: Date;

    @Column()
    createdTimestamp: number;

    @Column()
    totalBefore: number;
    @Column()
    memberDiscount: number;
    @Column()
    total: number;
    @Column()
    receiveAmount: number;
    @Column()
    change: number;
    @Column()
    paymentType: string;

    @Column()
    userId: number;
    @Column()
    user?: string;

    @Column()
    memberId: number;
    @Column()
    member?: string;
    @Column()
    memberPoint: number;
    @Column()
    point: number;
    @Column()
    receiptItems?: ReceiptItem[];
    @Column()
    promotion?: string;
    @Column()
    billType: 'salary' | 'sale' | 'WatElc' | 'rent' | 'material'

}
