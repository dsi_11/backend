/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BillsService {
  // constructor(@InjectRepository(Bill)
  // private BranchesRepository: Repository<Bill>,) { }

  bills: Bill[] = [{
    id: 1,
    queue: 1,
    createDate: new Date(),
    createdTimestamp: Date.now(),
    totalBefore: 100,
    memberDiscount: 10,
    total: 90,
    receiveAmount: 100,
    change: 10,
    paymentType: 'cash',
    userId: 1,
    memberId: 1,
    memberPoint: 50,
    point: 10,
    receiptItems: [
      {
        id: 1,
        name: 'Product 1',
        slcSubCate: 1,
        subCategory: 'Category 1',
        slcSweetLvl: 1,
        sweetLevel: 'Sweet',
        price: 10,
        unit: 2,
        productId: 101,
        product: 'Product A'
      },
      {
        id: 2,
        name: 'Product 2',
        slcSubCate: 2,
        subCategory: 'Category 2',
        slcSweetLvl: 2,
        sweetLevel: 'Very Sweet',
        price: 20,
        unit: 1,
        productId: 102,
        product: 'Product B'
      }
    ],
    billType: 'sale'
  }];

  create(createBillDto: CreateBillDto) {
    return 'This action adds a new bill';
  }

  findAll() {
    return this.bills;
  }

  findOne(id: number) {
    return `This action returns a #${id} bill`;
  }

  update(id: number, updateBillDto: UpdateBillDto) {
    return `This action updates a #${id} bill`;
  }

  remove(id: number) {
    return `This action removes a #${id} bill`;
  }
}
