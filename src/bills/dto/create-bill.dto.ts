/* eslint-disable prettier/prettier */
import type { ReceiptItem } from '../entities/bill.entity';
export class CreateBillDto {
    queue?: number;
    createDate: Date;
    createdTimestamp: number;
    totalBefore: number;
    memberDiscount: number;
    total: number;
    receiveAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: string;
    memberId: number;
    member?: string;
    memberPoint: number;
    point: number;
    receiptItems?: ReceiptItem[];
    promotion?: string;
    billType: 'salary' | 'sale' | 'WatElc' | 'rent' | 'material';
}
