/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Branch } from './entities/branch.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BranchesService {
  constructor(
    @InjectRepository(Branch)
    private BranchesRepository: Repository<Branch>,
  ) { }

  create(createBranchDto: CreateBranchDto): Promise<Branch> {
    return this.BranchesRepository.save(createBranchDto);
  }

  findAll(): Promise<Branch[]> {
    return this.BranchesRepository.find();
  }

  findOne(id: number) {
    return this.BranchesRepository.findOneBy({ id: id });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    await this.BranchesRepository.update(id, updateBranchDto);
    const Branch = await this.BranchesRepository.findOneBy({ id });
    return Branch;
  }

  async remove(id: number) {
    const deleteBranch = await this.BranchesRepository.findOneBy({ id });
    return this.BranchesRepository.remove(deleteBranch);
  }
}
