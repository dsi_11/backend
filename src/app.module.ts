import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemperatureModule } from './temperature/temperature.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { BranchModule } from './branch/branch.module';
import { Branch } from './branch/entities/branch.entity';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { BillsModule } from './bills/bills.module';
import { PromotionModule } from './promotion/promotion.module';
import { Material } from './material/entities/material.entity';
import { MaterialModule } from './material/material.module';
import { MemberModule } from './member/member.module';
import { Member } from './member/entities/member.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',
      entities: [Branch, Product, Material, Member],
      synchronize: true,
    }),
    TemperatureModule,
    UsersModule,
    ProductsModule,
    BranchModule,
    BillsModule,
    PromotionModule,
    MaterialModule,
    MemberModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
