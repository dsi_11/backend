import { IsNotEmpty, IsOptional } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  type: 'Drink' | 'Bakery' | 'Food';

  @IsOptional()
  subCategory?: 'Cold' | 'Hot' | 'Frappe' | '-';
}
