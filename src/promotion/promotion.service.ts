/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';

@Injectable()
export class PromotionService {
  promotions: Promotion[] = [
    {
      id: 1,
      name: 'มาม่าปลากะป๋อง',
      discount: 10,
      usePoint: 10,
    },
  ];
  create(createPromotionDto: CreatePromotionDto) {
    let lastId = this.promotions.length;
    const newPromotion = { ...createPromotionDto, id: ++lastId };
    this.promotions.push(newPromotion);
    return newPromotion;
  }

  findAll() {
    return this.promotions;
  }

  findOne(id: number) {
    const index = this.promotions.findIndex((promotion) => promotion.id === id);
    const findedPromotion = this.promotions[index];
    return findedPromotion;
  }

  update(id: number, updatePromotionDto: UpdatePromotionDto) {
    const index = this.promotions.findIndex((promotion) => promotion.id === id);
    this.promotions[index] = {
      ...this.promotions[index],
      ...updatePromotionDto,
    };
    return this.promotions[index];
  }

  remove(id: number) {
    const index = this.promotions.findIndex((promotion) => promotion.id === id);
    const deletedPromotion = this.promotions.splice(index, 1)[0];
    return deletedPromotion;
  }
}
