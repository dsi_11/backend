/* eslint-disable prettier/prettier */
import { IsNotEmpty, IsOptional } from 'class-validator';
export class CreatePromotionDto {
    @IsNotEmpty()
    name:string;
    
    @IsNotEmpty()
    discount:number;

    @IsNotEmpty()
    usePoint:number;

    @IsOptional()
    limit?:number;
}
