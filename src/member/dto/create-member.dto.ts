/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';
export class CreateMemberDto {
    @IsNotEmpty()
    id:number;
    
    @IsNotEmpty()
    name:string;

    @IsNotEmpty()
    tel:string;

    @IsNotEmpty()
    point:number;
}
