import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MaterialService {
  constructor(
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
  ) {}

  create(createMaterialDto: CreateMaterialDto): Promise<Material> {
    return this.materialRepository.save(createMaterialDto);
  }

  findAll(): Promise<Material[]> {
    return this.materialRepository.find();
  }

  findOne(id: number) {
    return this.materialRepository.findOneBy({ id: id });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    await this.materialRepository.update(id, updateMaterialDto);
    const Material = await this.materialRepository.findOneBy({ id });
    return Material;
  }

  async remove(id: number) {
    const deleteMaterial = await this.materialRepository.findOneBy({ id });
    return this.materialRepository.remove(deleteMaterial);
  }
}
