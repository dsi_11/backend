import { IsNotEmpty, IsOptional } from 'class-validator';
export class CreateMaterialDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  qty: number;

  @IsNotEmpty()
  min: number;

  @IsNotEmpty()
  unit: string;
}
